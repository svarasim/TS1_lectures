package cz.cvut.fel.ts1;

public class Svarasim {
    public long factorial (int n){
        long result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}
